//
//  AppNetwork.swift
//  FindTerestDEV
//
//  Created by Ramon Haro Marques on 07/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

class AppNetwork: NSObject{
    
    //MARK:- Properties
    //MARK: Constants
    enum AppNetworkError:Error {
        case dataCorrupted
    }
   
}



//MARK:- Network implementation
extension AppNetwork:Network{

    var venueRcomendations: String {
        guard let url = Bundle.main.url(forResource: "RecomendedVenuesResponse", withExtension: "json") else{ return ""}
        return url.absoluteString
    }
    var venueSearch:String{
        return "https://api.foursquare.com/v2/venues/search"
    }
    var venueDetails:String{
        return "https://api.foursquare.com/v2/venues/"
    }
    
    
    func fetchCodable<T>(urlRequest: URLRequest, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable, T : Encodable {
        
        guard let url = urlRequest.url else{
            completion(.failure(NetworkError.invalidURL))
            return
        }
        
        print("MOCKING...")
        
        do{
            let response = try JSONDecoder().decode(T.self, from: try Data(contentsOf: url))
            completion(.success(response))
        }
        catch{
            print(logClassName, "Error fetching data", error.localizedDescription)
            completion(.failure(error))
        }
        
    }
    
    func fetchData(urlRequest: URLRequest, completion: @escaping (Result<Data, Error>) -> Void) {
        
        //TODO
        
    }
    
    func send(data: Data, urlRequest: URLRequest, completion: @escaping (Error?) -> Void) {
        
        //TODO
        
    }
    
}
