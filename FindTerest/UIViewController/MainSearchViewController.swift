//
//  MainSearchViewController.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class MainSearchViewController: UIViewController {

    //MARK:- Properties
    //MARK: Constants
    let networkHelper:NetworkHelper
    let appLocationManager:AppLocationManager
    
    let disposeBag = DisposeBag()
    
    
    //MARK: Vars
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //UI
    lazy var headerImageView:UIImageView = {
        
        let rtView = UIImageView(image: UIImage(named: "main_search_header_image"))
        
        rtView.contentMode = .scaleAspectFill
        rtView.accessibilityIdentifier = "Main Header Image View"
        
        return rtView
        
    }()
    lazy var headerLabel:BaseLabel = {
        let rtView = BaseLabel(withConfiguration: .header)
        
        rtView.accessibilityIdentifier = "Header Label"
        rtView.textAlignment = .center
        rtView.numberOfLines = 0
        rtView.text = NSLocalizedString("messages.mainHeaderText", comment: "")
    
        return rtView
    }()
    lazy var searchView:SearchView = { SearchView(appLocationManager: appLocationManager) }()
    lazy var categoriesView:CategoriesView = { return CategoriesView() }()
    
    
    
    //MARK:- Constructor
    init(networkHelper:NetworkHelper = NetworkHelper.shared, appLocationManager:AppLocationManager = AppLocationManager.shared) {
        
        self.networkHelper = networkHelper
        self.appLocationManager = appLocationManager
        
        super.init(nibName: nil, bundle: nil)
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupClearNavigationBar()
        
    }
    
}


//MARK:- Private methods
private extension MainSearchViewController{
    
    func setupUI(){
        
        //Setup style
        view.backgroundColor = .secondaryViewBackground
        
        view.addSubview(headerImageView)
        headerImageView.anchor(top: view.topAnchor,
                               leading: view.leadingAnchor,
                               bottom: nil,
                               trailing: view.trailingAnchor,
                               size:.init(width: 0, height: (view.frame.height * 1) / 2))
        
        view.addSubview(searchView)
        searchView.anchor(top: headerImageView.bottomAnchor,
                          leading: nil,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: -64 / 2, left: 0, bottom: 0, right: 0),
                          width: view.widthAnchor,
                          widthMultiplier:0.75,
                          centerX: view.centerXAnchor,
                          size:.init(width: 0, height: 64))
        
        view.addSubview(headerLabel)
        headerLabel.anchor(top: nil,
                           leading: headerImageView.leadingAnchor,
                           bottom: searchView.topAnchor,
                           trailing: headerImageView.trailingAnchor,
                           padding: .init(top: Margins.xLarge, left: Margins.xLarge, bottom: 3 * Margins.xLarge, right:Margins.xLarge))
        
//        view.addSubview(categoriesView)
//        categoriesView.anchor(top: searchView.bottomAnchor,
//                              leading: view.leadingAnchor,
//                              bottom: view.bottomAnchor,
//                              trailing: view.trailingAnchor)

        setupBinders()
        
    }
    
    func setupBinders(){
        
        searchView.searchButton.rx.tap.bind{ [unowned self] in
            
            guard let searchString = self.searchView.searchViewViewModel.searchString.value else{ return }
            print(self.logClassName,"Search button clicked notification with search string = ", searchString)

//            #if DEV
//            let loaderVC = LoaderViewController()
//            LoadingOverlay.shared.showOverlay(view: UIApplication.shared.keyWindow!)
//            self.present(loaderVC, animated: true, completion: nil)
//            #endif
            
            self.showSearchResultViewController(searchCity: searchString)
            
            }
            .disposed(by: disposeBag)
        
    }
    
    func showSearchResultViewController(searchCity:String){
     
        navigationController?.pushViewController(SearchResultViewController(cityString: searchCity,
                                                                            networkHelper: networkHelper),
                                                 animated: true)
        
    }
    
}
