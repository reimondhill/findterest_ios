//
//  SearchResultViewController.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class SearchResultViewController: UIViewController {

    //MARK:- Properties
    //MARK:- Constants
    let cityString:String
    let networkHelper:NetworkHelper
    
    let disposeBag = DisposeBag()
    let requestLimit = 50
    
    
    //MARK:- Vars
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override var title: String?{
        didSet{
            scrollableTextNavigationBar.text = title ?? ""
        }
    }
    
    var venuesResponse:BehaviorRelay<VenuesResponse?> = BehaviorRelay(value: nil)
    var venues = BehaviorRelay(value: [Venue]())
    var isBusy = BehaviorRelay(value: false)
    
    //UI
    lazy var navigationSelectorButton:UIButton = {
        let rtView = addRightNavigationItem(withImage: UIImage(named: "map_ic")!)
        
        rtView.setImage(UIImage(named: "list_ic"), for: .selected)
        
        return rtView
    }()
    lazy var scrollableTextNavigationBar:ScrollableTextView = {
        let rtView = ScrollableTextView(withLabelConfiguration: .navBar)
        
        rtView.textAligment = .left
    
        return rtView
    }()
    
    lazy var messageLabel:BaseLabel = {
        let rtView = BaseLabel(withConfiguration: .normal)
        
        rtView.numberOfLines = 0
        rtView.textAlignment = .center
        
        return rtView
    }()
    lazy var indicator:UIActivityIndicatorView = {
        let rtView = UIActivityIndicatorView()
        rtView.color = UIColor.accentColor
        return rtView
    }()
    
    lazy var venueResultsView:VenueResultsView = {
        let rtView = VenueResultsView(items: venuesResponse.value?.groups.first?.items ?? [] )
    
        return rtView
    }()
    lazy var venuesMapView:VenuesMapView = {
        let rtView = VenuesMapView(venuesMapViewViewModel: VenuesMapView.VenuesMapViewViewModel(venues: venues.value))
        
        rtView.isHidden = true
        
        return rtView
    }()
    
    
    
    //MARK:- Constructor
    init(cityString:String, networkHelper:NetworkHelper = NetworkHelper.shared) {
        
        self.cityString = cityString
        self.networkHelper = networkHelper
        
        super.init(nibName: nil, bundle: nil)
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}


//MARK:- Lifecycle methods
extension SearchResultViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getVenues()
        navigationItem.titleView = scrollableTextNavigationBar
        title = String(format: NSLocalizedString("messages.venuesIn", comment: ""), cityString)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setDefaultNavigationBar()
        scrollableTextNavigationBar.refreshLabels()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let navigationBarBounds = navigationController?.navigationBar.bounds else{ return }
        scrollableTextNavigationBar.frame = navigationBarBounds
        
    }

}


//MARK:- Private methods
private extension SearchResultViewController{
    
    func setupUI(){
        
        view.backgroundColor = .mainViewBackground
        
        view.addSubview(venuesMapView)
        venuesMapView.constraintToSuperViewEdges()
        
        view.addSubview(venueResultsView)
        venueResultsView.constraintToSuperViewEdges()
        
        view.addSubview(indicator)
        indicator.anchor(top: nil,
                         leading: nil,
                         bottom: nil,
                         trailing: nil,
                         centerX: view.centerXAnchor,
                         centerY: view.centerYAnchor)
       
        
        view.addSubview(messageLabel)
        messageLabel.centerInSuperview()
        messageLabel.anchor(top: nil,
                            leading: view.leadingAnchor,
                            bottom: nil,
                            trailing: view.trailingAnchor,
                            padding: .init(top: 0, left: Margins.large, bottom: 0, right: Margins.large))
        
        setupBinders()
        
    }
    
    func setupBinders(){
        
        venuesResponse
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (venueResponse) in
                
                if let type = venueResponse?.groups.first?.type{
                    self.title = "\(type) in \(self.cityString)"
                }
                
                if let venueResponse = venueResponse{
                    
                    var tmpVenue:[Venue] = []
                    for item in venueResponse.groups.first?.items ?? []{
                        guard let venue = item.venue else{ continue }
                        
                        tmpVenue.append(venue)
                    }
                    
                    self.messageLabel.isHidden = tmpVenue.count == 0
                    self.messageLabel.text = tmpVenue.count == 0 ? String(format: NSLocalizedString("messages.noVenuesFound", comment: ""), self.cityString):""
                    self.venues.accept(tmpVenue)
                }
                else{
                    self.venues.accept([])
                    self.venuesMapView.venuesMapViewViewModel.venues.accept([])
                    self.messageLabel.isHidden = false
                    self.messageLabel.text = NSLocalizedString("messages.somethingWrong", comment: "")
                }
                
                self.venueResultsView.items.accept(venueResponse?.groups.first?.items ?? [])
                
            })
            .disposed(by: disposeBag)
        
        venues
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (venues) in
                self.venuesMapView.venuesMapViewViewModel.venues.accept(venues)
            })
            .disposed(by: disposeBag)
        
        navigationSelectorButton.rx
            .tap
            .subscribe(onNext: { [unowned self] _ in
                
                self.navigationSelectorButton.isSelected = !self.navigationSelectorButton.isSelected
                
                self.venuesMapView.isHidden = !self.navigationSelectorButton.isSelected
                self.venueResultsView.isHidden = self.navigationSelectorButton.isSelected
                
            })
            .disposed(by: disposeBag)
        
        isBusy
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (isBusy) in
                
                print(self.logClassName, "isBusy = ", isBusy)
                if isBusy{
                    self.messageLabel.isHidden = true
                    
                    self.venueResultsView.isHidden = true
                    self.venuesMapView.isHidden = true
                    
                    self.indicator.isHidden = false
                    self.indicator.startAnimating()
                }
                else{
                
                    self.venuesMapView.isHidden = !self.navigationSelectorButton.isSelected
                    self.venueResultsView.isHidden = self.navigationSelectorButton.isSelected
                    
                    self.indicator.isHidden = true
                    self.indicator.stopAnimating()
                }
                
            }).disposed(by: disposeBag)
        
    }
    
    func getVenues(){
        
        //MAX Free Results
        isBusy.accept(true)
        networkHelper.getVenueRecomendations(inLocation: cityString, offset: 0, limit: requestLimit) { [weak self] (venuesSearchResult) in
            
            guard let strongSelf = self else{ return }
            
            switch venuesSearchResult{
            case .success(let venuesResponse):
                print(strongSelf.logClassName, "Got venues success")
                strongSelf.venuesResponse.accept(venuesResponse)
            case .failure(let venuesSearchResultError):
                print(strongSelf.logClassName, "ERROR getting venues", venuesSearchResultError)
                strongSelf.venuesResponse.accept(nil)
            }
            
            strongSelf.isBusy.accept(false)
 
        }
        
    }
    
}


