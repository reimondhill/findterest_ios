//
//  MainBaseTableViewCell.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 03/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//


import UIKit


class MainBaseTableViewCell: UITableViewCell {
    
    //MARK:- Properties
    //MARK: Vars
    var selectedView:UIView!
    
    
    
    //MARK:- Lifecycle methods
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        selectedView = UIView()
        selectedView.backgroundColor = UIColor.mainTableViewCellBackground
        selectedBackgroundView = selectedView
        
        //The table view background defines the color
        contentView.backgroundColor = .clear
        
    }
    
    
}
