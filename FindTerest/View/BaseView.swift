//
//  BaseView.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//


import UIKit

class BaseView: UIView {

    //MARK:- Properties
    //MARK: Variables
    @IBInspectable var cornerRadius:CGFloat = BorderRadius.medium{
        didSet{
            layer.cornerRadius = cornerRadius
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = Separators.xSmall{
        didSet{
            layer.borderWidth = borderWidth
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.mainBorder{
        didSet{
            layer.borderColor = borderColor.cgColor
            setNeedsDisplay()
        }
    }
    
    
    
    //MARK:- Lifecicle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    
    
    //MARK:- Private methods
    private func setupView(){
        
        backgroundColor = UIColor.mainViewBackground
        addBorder(borderWidth: borderWidth, radius: cornerRadius, color: borderColor)
        
    }
    
    
    
    //MARK:- Public methods




}
