//
//  ScrollableTextView.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import QuartzCore


class ScrollableTextView: UIView, UIScrollViewDelegate {
    
    //MARK:- Variables
    //MARK: Constants
    private let defaultNumberOfLabels:Int = 2
    private let defaultFadeLength:CGFloat = 7.0
    private let defaultPixelsPerSecond:CGFloat = 30
    private let defaultPauseTime:CGFloat = 1.5
    private let defaultLabelBufferSpace:CGFloat = 20
    
    
    //MARK: Vars
    //Customisation vars
    ///Text to display
    var text:String{
        set{
            if newValue != text{
                for label in labels{
                    label.text = newValue
                }
                refreshLabels()
            }
        }
        get{
            return mainLabel.text ?? ""
        }
    }
    
    var labelConfiguration:BaseLabel.LabelConfiguration = .normal{
        didSet{
            for label in labels{ label.labelConfigurationOption = labelConfiguration.rawValue }
        }
    }
    
    var attributedText:NSAttributedString{
        set{
            
            if newValue.string != attributedText.string{
                
                for label in labels{
                    label.attributedText = newValue
                }
                
            }
            
            refreshLabels()
            
        }
        get{
            
            return mainLabel.attributedText ?? NSAttributedString()
            
        }
    }
    var textColor:UIColor{
        set{
            
            for label in labels{
                label.textColor = newValue
            }
            
        }
        get{
            
            return mainLabel.textColor
            
        }
    }
    
    var textAligment = NSTextAlignment.left
    
    var font:UIFont{
        set{
            
            if newValue != font{
            
                for label in labels{
                    label.font = newValue
                }
                
            }
            
            refreshLabels()
            //self.invalidateIntrinsicContentSize()
        }
        get{
            return mainLabel.font
        }
    }
    var fontSize:CGFloat{
        set{
            for label in labels{
                label.font = label.font.withSize(newValue)
            }
        }
        get{
            return mainLabel.font.pointSize
        }
    }
    var shadowColor:UIColor{
        set{
            for label in labels{
                label.shadowColor = newValue
            }
        }
        get{
            return mainLabel.shadowColor ?? .black
        }
    }
    var shadowOffset:CGSize{
        set{
            for label in labels{
                label.shadowOffset = newValue
            }
        }
        get{
            return mainLabel.shadowOffset
        }
    }
    
    var pauseInterval = TimeInterval()
    var fadeLength:CGFloat = 0{
        didSet{
            refreshLabels()
        }
    }
    
    var scrollSpeed:CGFloat = 0{
        didSet{
            refreshLabels()
        }
    }
    var scrollDirection = Direction.right{
        didSet{
            refreshLabels()
        }
    }
    
    
    //Override variables
    override internal var intrinsicContentSize: CGSize{
        get{
            return CGSize.init(width: 0, height: mainLabel.intrinsicContentSize.height)
        }
    }
    
    
    //Private variables
    private var labelSpacing:CGFloat = 0
    private var animationOption = UIView.AnimationOptions.curveLinear
    
    private lazy var labels = [BaseLabel]()
    var mainLabel:BaseLabel{
        get{
            return labels.first ?? BaseLabel(withConfiguration: labelConfiguration)
        }
    }
    
    private var isScrolling = false
    lazy private var scrollView:UIScrollView = {
        
        let aScrollView = UIScrollView(frame: self.bounds)
        aScrollView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        self.addSubview(aScrollView)
        
        return aScrollView
        
    }()
    
    
    
    //MARK:- Init
    init(withLabelConfiguration labelConfiguration:BaseLabel.LabelConfiguration){
        
        self.labelConfiguration = labelConfiguration
        super.init(frame: CGRect())
        initialize()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
        
    }
    
    deinit {
        
        print("\(logClassName) Deinit")
        
    }
    
    private func initialize(){
        
        for _ in 0..<defaultNumberOfLabels{
            
            let label = BaseLabel(withConfiguration: labelConfiguration)
            
            label.backgroundColor = .clear
            label.autoresizingMask = self.autoresizingMask
            
            scrollView.addSubview(label)
            labels.append(label)
            
        }
        
        scrollDirection = .left
        scrollSpeed = defaultPixelsPerSecond
        
        pauseInterval = TimeInterval(defaultPauseTime)
        labelSpacing = defaultLabelBufferSpace
        fadeLength = defaultFadeLength
        textAligment = NSTextAlignment.left
        animationOption = UIView.AnimationOptions.curveLinear
        
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isScrollEnabled = false
        scrollView.isUserInteractionEnabled = false
        
        self.backgroundColor = .clear
        self.clipsToBounds = true
        
    }
    
    
    
    //MARK:- Lifecycle Methods
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if superview != nil{
            
            print("\(logClassName) Added to superview Frame: \(frame)")
            refreshLabels()
            
        }
        else{
            
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            NotificationCenter.default.removeObserver(self)
        
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        refreshLabels()

    }
    

    
    //MARK:- Private methods
    func refreshLabels(){
        
        var offset:CGFloat = 0
        
        for label in labels{
            
            label.sizeToFit()
            
            var frame = label.frame
            frame.origin = CGPoint(x: offset, y: 0)
            frame.size.height = self.bounds.height
            label.frame = frame
            
            label.center = CGPoint(x: label.center.x, y: CGFloat(roundf(Float(self.center.y - self.frame.minY))))
            
            offset += label.bounds.width + labelSpacing
            
        }
        
        scrollView.contentOffset = CGPoint.zero
        scrollView.layer.removeAllAnimations()
        
        if mainLabel.bounds.width > self.bounds.width{
            
            let newWidth = mainLabel.bounds.width + self.bounds.width + labelSpacing
            let newHeigth = self.bounds.height
            
            scrollView.contentSize = CGSize(width: newWidth, height: newHeigth)
            
            for label in labels{
                label.isHidden = false
            }
            
            applyGradientMaskForFadeLength(fadeLength: fadeLength, enableFade: isScrolling)
            scrollLabelIfNeeded()
            
        }
        else{
            
            for label in labels{
                label.isHidden = mainLabel != label
            }
            
            scrollView.contentSize = self.bounds.size
            mainLabel.frame = self.bounds
            mainLabel.isHidden = false
            mainLabel.textAlignment = textAligment
            
            scrollView.layer.removeAllAnimations()
            applyGradientMaskForFadeLength(fadeLength: 0, enableFade: false)
            
        }
        
    }
    
    @objc func scrollLabelIfNeeded(){
        
        if text.count > 0{
            
            let labelWidth = mainLabel.bounds.width
            
            if labelWidth > self.bounds.width{
                
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(scrollLabelIfNeeded), object: nil)
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(enableShadow), object: nil)
                
                let doScrollLeft = scrollDirection == .left
                scrollView.layer.removeAllAnimations()
                scrollView.contentOffset = doScrollLeft ? CGPoint.zero : CGPoint(x: labelWidth + labelSpacing, y: 0)
                
                perform(#selector(enableShadow), with: nil, afterDelay: pauseInterval)
                
                let duration:TimeInterval = TimeInterval(labelWidth/scrollSpeed)
                UIView.animate(withDuration: duration, delay: pauseInterval, options: [animationOption, .allowUserInteraction], animations: {
                    
                    self.scrollView.contentOffset = doScrollLeft ? CGPoint.init(x: labelWidth + self.labelSpacing, y: 0): CGPoint.zero
                    
                }, completion: { (finished) in
                    
                    self.isScrolling = false
                    self.applyGradientMaskForFadeLength(fadeLength: self.fadeLength, enableFade: false)
                    if finished{
                        self.perform(#selector(self.scrollLabelIfNeeded), with: nil)
                    }
                    
                })
                
            }
            
        }
        
    }
    
    @objc private func enableShadow(){
        
        isScrolling = true
        applyGradientMaskForFadeLength(fadeLength: fadeLength, enableFade: true)
        
    }
    
    private func applyGradientMaskForFadeLength(fadeLength:CGFloat, enableFade fade:Bool){
        
        let labelWidth = mainLabel.bounds.width
        
        if labelWidth > self.bounds.width{
            
            let gradientMask = CAGradientLayer.init(layer: layer)
            
            gradientMask.bounds = self.layer.bounds
            gradientMask.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
            
            gradientMask.shouldRasterize = true
            gradientMask.rasterizationScale = UIScreen.main.scale
            
            gradientMask.startPoint = CGPoint(x: 0, y: self.frame.midY)
            gradientMask.endPoint = CGPoint(x: 1, y: self.frame.midY)
            
            gradientMask.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
            
            let fadePoint:CGFloat = fadeLength / self.bounds.width
            var leftFadePoint:NSNumber = NSNumber.init(value: Float(fadePoint))
            var rightFadePoint:NSNumber = NSNumber.init(value: Float(1 - fadePoint))
            
            if !fade{
                
                switch scrollDirection{
                case .left:
                    leftFadePoint = 0
                case .right:
                    leftFadePoint = 0
                    rightFadePoint = 1
                default:
                    break
                }
                
            }
            
            gradientMask.locations = [0, leftFadePoint, rightFadePoint, 1]
            
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            
            self.layer.mask = gradientMask
            
            CATransaction.commit()
            
        }
        else{
            self.layer.mask = nil
        }
        
    }
    
}
