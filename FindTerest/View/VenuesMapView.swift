//
//  VenuesMapView.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 04/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa


class VenuesMapView: UIView {

    //Another possibility for MVVM
    //MARK:- ViewModel
    class VenuesMapViewViewModel:NSObject{
        
        var venues:BehaviorRelay<[Venue]>
        
        init(venues:[Venue]) {
            
            self.venues = BehaviorRelay(value: venues)
            super.init()
            
        }
        
    }
    
    //MARK:- Properties
    //MARK: Constants
    let venuesMapViewViewModel:VenuesMapViewViewModel
    
    let disposeBag = DisposeBag()
    let regionRadius: CLLocationDistance = 1000
    
    
    //MARK: Vars
    lazy var mapView:MKMapView = {
        let rtView = MKMapView()
        
        rtView.delegate = self
        rtView.showsUserLocation = true
        rtView.userTrackingMode = MKUserTrackingMode.followWithHeading
        
        return rtView
    }()
    
    
    
    //MARK:- Constructor
    init(venuesMapViewViewModel:VenuesMapViewViewModel) {
        
        self.venuesMapViewViewModel = venuesMapViewViewModel
        super.init(frame: .zero)
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}


//MARK:- Lifecycle methods
extension VenuesMapView{
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if superview != nil{
            
            venuesMapViewViewModel.venues
                .asObservable()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [unowned self] (venues) in
                    
                    self.addMapAnotations(venues: venues)
                    
                })
                .disposed(by: disposeBag)
            
        }
        
    }
    
}


//MARK:- Private methods
private extension VenuesMapView{
    
    func setupUI(){
        
        addSubview(mapView)
        mapView.constraintToSuperViewEdges()
        
    }
    
    func addMapAnotations(venues:[Venue]){
        
        //TODO
        //mapView.register(, forAnnotationViewWithReuseIdentifier: )
        mapView.removeAnnotations(mapView.annotations)
        //Genting the first available cordinate location as a center
        var locationCoordinate2D:CLLocationCoordinate2D?
        
        for venue in venues{
            
            guard let title = venue.name,
                let location = venue.location,
                let lat = location.latitud,
                let lng = location.longitud else{ continue }
            
            let annotation = MKPointAnnotation()
            annotation.title = title
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            
            if locationCoordinate2D == nil { locationCoordinate2D = annotation.coordinate }
                
            mapView.addAnnotation(annotation)
            
        }
        
        let viewRegion = MKCoordinateRegion(center: locationCoordinate2D ?? CLLocationCoordinate2D(), latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(viewRegion, animated: false)
        mapView.showsUserLocation = true
        
    }

}


extension VenuesMapView:MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
       
        guard annotation is MKPointAnnotation else { return nil }
        
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        annotationView.canShowCallout = true
        
        return annotationView
        
    }
    
}
