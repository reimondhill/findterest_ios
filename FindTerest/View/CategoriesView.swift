//
//  CategoriesView.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 06/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

class CategoriesView: UIView {

    //TODO Not ready yet
    //MARK:- Properties
    let categories = [["Breakfast", "Lunch", "Dinner"],
                        ["Coffe", "Nightlife", "Other"]]
    
    //MARK: Vars
    lazy var categoriesCollectionView:UICollectionView = {

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = .zero
        layout.itemSize = CGSize(width: frame.width/3, height: frame.height/2)
        //collectionViewLayout.collectionViewContentSize
        let rtView = UICollectionView(frame: frame, collectionViewLayout: layout)
        
        rtView.contentMode = .scaleAspectFit
        rtView.register(CategoriesCollectionViewCell.self, forCellWithReuseIdentifier: CategoriesCollectionViewCell.identifier)
        rtView.backgroundColor = .green
        rtView.dataSource = self
        
        return rtView
    }()
    
}


//MARK:Lifecycle methods
extension CategoriesView{
    
    override func didMoveToWindow() {
        
        if superview != nil{

            backgroundColor = .clear
            addSubview(categoriesCollectionView)
            
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        categoriesCollectionView.performBatchUpdates(nil, completion: nil)
        
    }
    
}


extension CategoriesView:UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let categoryCell:CategoriesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoriesCollectionViewCell.identifier, for: indexPath) as! CategoriesCollectionViewCell
        
        categoryCell.categoryType = categories[indexPath.section][indexPath.row]
        
        return categoryCell
        
    }
    
}


extension CategoriesView:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        
        let width = collectionView.bounds.width/3
        let height = collectionView.bounds.height / 2
        
        return CGSize(width: width, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return .zero
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
}
