//
//  VenueTableViewCell.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 03/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import SDWebImage

class VenueTableViewCell: UITableViewCell {

    //MARK:- Properties
    //MARK: Vars
    //MVVM without RxSwift. A different approach.
    var venueTableViewCellViewModel:VenueTableViewCellViewModel?{
        didSet{
            updateUI(venueTableViewCellViewModel: venueTableViewCellViewModel)
        }
    }
    
    //UI
    lazy var iconImageView:UIImageView = {
        let rtView = UIImageView()
        
        rtView.contentMode = .scaleAspectFit
        rtView.tintColor = UIColor.defaultTint
        
        return rtView
    }()
    lazy var distanceLabel:BaseLabel = {
        let rtView = BaseLabel(withConfiguration: .normalSmall)
        
        rtView.textAlignment = .center
        rtView.numberOfLines = 2
        
        return rtView
    }()
    lazy var reasonLabel:BaseLabel = {
        let rtView = BaseLabel(withConfiguration: .normal)
        
        rtView.textAlignment = .center
        rtView.numberOfLines = 2
        
        return rtView
    }()
    lazy var nameLabel:BaseLabel = {
        let rtView = BaseLabel(withConfiguration: .title)
        
        rtView.textAlignment = .center
        rtView.numberOfLines = 0
        
        return rtView
    }()
    lazy var addresLabel:BaseLabel = {
        let rtView = BaseLabel(withConfiguration: .normalSmall)
        
        rtView.textAlignment = .center
        rtView.numberOfLines = 0
        
        return rtView
    }()
    
    
}

//MARK:- Lifecycle methods
extension VenueTableViewCell{
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if superview != nil{
            setupUI()
        }
        
    }
    
    override func prepareForReuse() {
        updateUI(venueTableViewCellViewModel: nil)
    }
    
}


//MARK:- Private methods
private extension VenueTableViewCell{
    
    func setupUI(){
        
        addSubview(iconImageView)
        iconImageView.anchor(top: topAnchor,
                             leading: leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: Margins.medium, left: Margins.large, bottom: 0, right: 0),
                             size: .init(width: 42, height: 42))
        
        addSubview(distanceLabel)
        distanceLabel.anchor(top: iconImageView.bottomAnchor,
                             leading: iconImageView.leadingAnchor,
                             bottom: nil,
                             trailing: iconImageView.trailingAnchor,
                             padding: .init(top: Margins.medium, left: 0, bottom: 0, right: 0))

        addSubview(nameLabel)
        nameLabel.anchor(top: iconImageView.topAnchor,
                         leading: iconImageView.trailingAnchor,
                         bottom: nil,
                         trailing: trailingAnchor,
                         padding: .init(top: 0, left: Margins.medium, bottom: 0, right: Margins.medium),
                         size: .init(width: 0, height: 23))

        addSubview(reasonLabel)
        reasonLabel.anchor(top: nameLabel.bottomAnchor,
                           leading: nameLabel.leadingAnchor,
                           bottom: nil,
                           trailing: nameLabel.trailingAnchor)

        addSubview(addresLabel)
        addresLabel.anchor(top: reasonLabel.bottomAnchor,
                           leading: nameLabel.leadingAnchor,
                           bottom: bottomAnchor,
                           trailing: nameLabel.trailingAnchor,
                           padding: .init(top: 0, left: 0, bottom: Margins.medium, right: 0),
                           size: .init(width: 0, height: 38))
        
    }
    
    func updateUI(venueTableViewCellViewModel:VenueTableViewCellViewModel?){
        
        iconImageView.sd_setImage(with: venueTableViewCellViewModel?.venueIconURL) { [weak self] (image, _, _, _) in
            guard let strongSelf = self else{ return }
            strongSelf.iconImageView.image = strongSelf.iconImageView.image?.imageWithColor(UIColor.defaultTint)
        }
        distanceLabel.text = venueTableViewCellViewModel?.distanceKMString
        nameLabel.text = venueTableViewCellViewModel?.nameVenue
        reasonLabel.text = venueTableViewCellViewModel?.reasonString
        addresLabel.text = venueTableViewCellViewModel?.addressVenue

        
    }
    
}
