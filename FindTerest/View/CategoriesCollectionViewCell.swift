//
//  CategoriesCollectionViewCell.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 06/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {

    //MARK:- Properties
    //MARK: Vars
    override var isSelected: Bool {
        didSet {
            
            let view = UIView(frame:self.bounds)
            view.backgroundColor = isSelected ? UIColor.mainTableViewCellBackground : .clear
            self.selectedBackgroundView = view
            
        }
        
    }
    var categoryType:String? = nil {
        didSet{
            updateView(categoryType: categoryType)
        }
    }
    
    //UI
    lazy var iconImageView:UIImageView = {
        let rtView = UIImageView()
        
        rtView.contentMode = .scaleAspectFit
        
        return rtView
    }()
    lazy var titleLabel:BaseLabel = {
       let rtView = BaseLabel(withConfiguration: .normal)
        
        rtView.textAlignment = .center
        
        return rtView
    }()
    
}


//Mark:- Lifecycle methods
extension CategoriesCollectionViewCell{
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if superview != nil{
            
            setupUI()
        }
        
    }
    
}


//MARK:- Private methods
extension CategoriesCollectionViewCell{
    
    func setupUI(){
    
        addSubview(iconImageView)
        iconImageView.anchor(top: nil,
                             leading: nil,
                             bottom: nil,
                             trailing: nil,
                             centerX: centerXAnchor,
                             centerY: centerYAnchor,
                             size: .init(width: frame.width/2, height: frame.height/2))
        
        addSubview(titleLabel)
        titleLabel.anchor(top: iconImageView.bottomAnchor,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: trailingAnchor,
                          padding: .init(top: Margins.large, left: 0, bottom: 0, right: 0))
        
    }
    
    func updateView(categoryType:String?){
        
        if let categoryType = categoryType{
            titleLabel.text = categoryType
            
            var categoryType = categoryType
            categoryType = categoryType.lowercased()
            categoryType += "_ic"
            
            iconImageView.image = UIImage(named: categoryType)
        }
        else{
            iconImageView.image = nil
            titleLabel.text = nil
        }
    }
    
}
