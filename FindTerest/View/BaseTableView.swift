//
//  MainTableView.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 03/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

class BaseTableView: UITableView {

    //MARK:- Variables
    //MARK: Constants
    enum TableConfiguration:Int{
        case normal = 0
    }
    
    
    //MARK: Vars
    var tableConfiguration:TableConfiguration = .normal{
        didSet{
            initialize()
        }
    }
    
    @IBInspectable var tableConfigurationOption:Int{
        
        get{
            return tableConfiguration.rawValue
        }
        set{
            guard let tableConfigurationTemp = TableConfiguration(rawValue: newValue) else{ return }
            tableConfiguration = tableConfigurationTemp
        }
        
    }
    
    
    
    //MARK:- Constructor
    init(withConfiguration tableConfiguration:TableConfiguration) {
        super.init(frame: CGRect(), style:.plain)
        
        initialize()
        
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        initialize()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        
        initialize()
        
    }
    
    
    override func layoutMarginsDidChange() {
        super.layoutMarginsDidChange()
        
        layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        separatorInset = .zero
        allowsMultipleSelection = false
        
    }
    
    
    
    //MARK:- Private methods
    private func initialize(){
        
        bounces = false
        tableFooterView = UIView()
        backgroundColor = UIColor.mainTableViewBackground
        
    }
    
}
