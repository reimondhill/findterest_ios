//
//  VenueResultsView.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 04/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class VenueResultsView: UIView {

    //MARK:- Properties
    //MARK: Constants
    let disposeBag = DisposeBag()
    
    //MARK: Vars
    var items:BehaviorRelay<[Item]>
    
    //UI
    lazy var venuesTableView:BaseTableView = {
        let rtView = BaseTableView(withConfiguration: .normal)
        
        rtView.register(VenueTableViewCell.self, forCellReuseIdentifier: VenueTableViewCell.identifier)
        rtView.delegate = self
        
        return rtView
    }()
    
    
    
    //MARK:- Constructor
    init(items:[Item]) {
        
        self.items = BehaviorRelay(value: items)
        super.init(frame: .zero)
        
        setupUI()
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}


//MARK:- Private methods
private extension VenueResultsView{
    
    func setupUI(){
        
        addSubview(venuesTableView)
        venuesTableView.constraintToSuperViewEdges()
        
        setupBinders()
    }
    
    func setupBinders(){
        
        
            items.observeOn(MainScheduler.instance)
            .bind(to: venuesTableView.rx.items(cellIdentifier: VenueTableViewCell.identifier, cellType: VenueTableViewCell.self)) {(row, element, cell) in
                cell.venueTableViewCellViewModel = VenueTableViewCellViewModel(item: element)
            }
            .disposed(by: disposeBag)
        
    }
    
}


//MARK:- UITableview implementation
//MARK: UITableViewDelegate implementation
extension VenueResultsView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewControler?.currentTraitStatus == .wRhR ? 130:120
    }
    
}

