//
//  BaseLabel.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {

    //MARK:- Properties
    //MARK: Constants
    enum LabelConfiguration:Int, CaseIterable{
        
        case title = 7
        case normal = 0
        case normalSmall = 1
        
        case normalHigligthed = 2
        case normalSmallHigligthed = 3
        
        case navBar = 4
        case header = 5
        case subHeader = 6
    
        //More future cases
    }
    
    
    //MARK: Variables
    var labelConfiguration:LabelConfiguration = .normal{
        didSet{
            setupView()
        }
    }
    @IBInspectable var labelConfigurationOption:Int{
        get{
            return labelConfiguration.rawValue
        }
        set{
            guard let labelConfigurationTemp = LabelConfiguration(rawValue: newValue) else{ return }
            labelConfiguration = labelConfigurationTemp
        }
    }
    
    
    
    //MARK:- Constructor
    init(withConfiguration labelConfiguration:LabelConfiguration) {
        
        self.labelConfiguration = labelConfiguration
        super.init(frame: CGRect())
        
        backgroundColor = .clear
        setupView()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    
    
    //MARK:- Lifecycle methods
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //Future if handling different label configurations for portrait and landscape
        
    }
    
    
    //MARK:- Private methods
    private func setupView(){
        
        switch labelConfiguration {
            
        case .title:
            textColor = UIColor.mainTextColor
            font = UIFont.systemFont(ofSize: TextSize.title, weight: .bold)
        case .normal, .normalHigligthed:
            textColor = labelConfiguration == .normal ? UIColor.mainTextColor:UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: TextSize.normal, weight: .regular)
   
        case .normalSmall, .normalSmallHigligthed:
            textColor = labelConfiguration == .normalSmall ? UIColor.mainTextColor:UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: TextSize.normalSmall, weight: .light)

        case .header, .subHeader:
            textColor = UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: labelConfiguration == .header ? TextSize.heaader:TextSize.subHeader,
                                     weight: labelConfiguration == .header ? .heavy:.semibold)
        case .navBar:
            textColor = UIColor.navigationBarText
            font = UIFont.systemFont(ofSize: TextSize.navigationTitle, weight: .semibold)
            
        }
        
    }
    
}
