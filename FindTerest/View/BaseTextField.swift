//
//  BaseTextField.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

class BaseTextField: UITextField {

    //MARK:- Properties
    //MARK: Constants
    enum TextFieldConfiguration:Int{
        case normal = 0
    }
    
    
    //MARK: Vars
    var textFieldConfiguration:TextFieldConfiguration = .normal{
        didSet{
            setupView()
        }
    }
    @IBInspectable var textFieldConfigurationOption:Int{
        get{
            return textFieldConfiguration.rawValue
        }
        set{
            guard let textFieldConfigurationTemp = TextFieldConfiguration(rawValue: newValue) else{ return }
            textFieldConfiguration = textFieldConfigurationTemp
        }
    }
    
    @IBInspectable var placeholderText:String = ""{
        didSet{
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .center
            
            switch textFieldConfiguration {
            case .normal:
                attributedPlaceholder = NSAttributedString(string:placeholderText,
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.textFieldPlaceholder,
                                                                        NSAttributedString.Key.paragraphStyle:paragraph])
            }
            
        }
    }
    
    
    
    //MARK:- Constructor
    init(withConfiguration textFieldConfiguration: TextFieldConfiguration) {
        super.init(frame: CGRect())
        
        textFieldConfigurationOption = textFieldConfiguration.rawValue
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    
    //MARK:- Private methods
    private func setupView(){
        
        switch textFieldConfiguration {
        case .normal:
            backgroundColor = .clear
            textColor = .mainTextColor
            font = UIFont.systemFont(ofSize: TextSize.normal, weight: .regular)
        }
        
    }
    
    
    
    //MARK:- Public methods
    
    
    
    
}
