//
//  SearchView.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class SearchView: UIView {
    
    //MARK:- Properties
    //MARK: Constants
    let disposeBag = DisposeBag()
    
    
    //MARK: Vars
    var searchViewViewModel:SearchViewViewModel
    
    //UI
    lazy var searchButton: UIButton = {
        let rtView = UIButton()
        
        rtView.setImage(UIImage(named: "search_ic"), for: .normal)
        rtView.contentMode = .scaleAspectFit
        rtView.accessibilityIdentifier = "Search Locations Button"
        
        return rtView
    }()
    lazy var searchTextField: BaseTextField = {
        let rtView = BaseTextField(withConfiguration: .normal)
        
        rtView.accessibilityIdentifier = "Search Locations Input"
        
        return rtView
    }()
    
    
    
    //MARK:- Constructor
    init(appLocationManager:AppLocationManager = AppLocationManager.shared) {
        
        self.searchViewViewModel = SearchViewViewModel(appLocationManager: appLocationManager)
        super.init(frame: .zero)
        initUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


//MARK:- Lifecycle methods
extension SearchView{
    
    override func didMoveToSuperview() {
        
        if superview != nil{
            searchViewViewModel.appLocationManager.startLocationUpdate()
        }
        else{
            searchViewViewModel.appLocationManager.stopLocationUpdate()
        }
        
    }
    
}


private extension SearchView{
    
    func initUI(){
        accessibilityIdentifier = "Search View"
        
        backgroundColor = .mainViewBackground
        addBorder(borderWidth: Separators.xSmall, radius: BorderRadius.medium, color: .mainBorder)
        tintColor = .defaultTint
    
        addSubview(searchButton)
        searchButton.anchor(top: nil,
                            leading: nil,
                            bottom: nil,
                            trailing: trailingAnchor,
                            padding: .init(top: 0, left: 0, bottom: 0, right: Margins.medium),
                            centerY: centerYAnchor,
                            size: .init(width: 32, height: 32))
        
        addSubview(searchTextField)
        searchTextField.anchor(top: topAnchor,
                               leading: leadingAnchor,
                               bottom: bottomAnchor,
                               trailing: searchButton.leadingAnchor,
                               padding: .init(top: Margins.medium, left: Margins.medium, bottom: Margins.medium, right: Margins.medium))
        
        setupBinders()
        
    }
    
    func setupBinders(){
        
        searchViewViewModel.nearByCity
            .asObservable()
            .distinctUntilChanged()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (city) in
                if let city = city{
                    self.searchTextField.placeholder = String(format: NSLocalizedString("messages.searchNearby", comment: ""),city)
                }
                else{
                    self.searchViewViewModel.appLocationManager.startLocationUpdate()
                    self.searchTextField.placeholder = String(format: NSLocalizedString("messages.searchNearby", comment: ""),"")
                }
            })
            .disposed(by: disposeBag)
        
        Observable.combineLatest(searchViewViewModel.nearByCity, searchTextField.rx.text)
            .asObservable()
            .map({ (nearByCity, searchTextString) -> (String?, String) in
                return (nearByCity, searchTextString ?? "")
            })
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (nearByCity, searchTextString) in
                
                if nearByCity != nil || searchTextString != ""{
                    self.searchButton.isEnabled = true
                    self.searchViewViewModel.searchString.accept(searchTextString != "" ? searchTextString:nearByCity)
                }
                else{
                    self.searchButton.isEnabled = false
                    self.searchViewViewModel.searchString.accept(nil)
                }
                
            })
            .disposed(by: disposeBag)
        
    }
    
}



