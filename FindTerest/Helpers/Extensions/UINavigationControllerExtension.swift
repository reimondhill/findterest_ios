//
//  UINavigationControllerExtension
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    //MARK:- Properties
    override open var preferredStatusBarStyle : UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
    
}
