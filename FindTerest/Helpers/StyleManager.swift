//
//  StyleManager.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

extension UIColor{
    
    //MARK:- App Colors Extension
    static var accentColor:UIColor {
        return UIColor(red: 250.0/255.0, green: 77.0/255.0, blue: 25.0/255.0, alpha: 1)
    }
    
    static var defaultSeparator:UIColor {
        return UIColor.lightGray
    }
    
    static var defaultTint:UIColor{
        return mainTextColor
    }
    
    
    //MARK: Status, Toolbar and NavigationBar
    static var statusBarBackground:UIColor{
        return accentColor
    }
    
    static var navigationBarBackground:UIColor{
        return accentColor
    }
    static var navigationBarTint:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    static var navigationBarText:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    static var toolbar:UIColor{
        return accentColor
    }
    static var toolbarTint:UIColor{
        return .white
    }
    
    
    //MARK: UIView
    static var mainViewBackground:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    static var secondaryViewBackground:UIColor{
        return UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
    }
    
    static var mainBorder:UIColor{
        return UIColor.clear
    }
    
    static var secondaryBorder:UIColor{
        return UIColor.clear
    }
    
    static var cellViewBackground:UIColor{
        return secondaryViewBackground
    }
    
    static var cellViewSelectedBackground:UIColor{
        return accentColor
    }
    
    static var mainButtonBackground:UIColor{
        return accentColor
    }
    
    static var mainButtonBorder:UIColor{
        return mainButtonBackground
    }
    
    static var mainButtonDisabledBackground:UIColor{
        return .lightGray
    }
    
    static var mainButtonDissabledBorder:UIColor{
        return mainButtonDisabledBackground
    }
    
    static var secondaryButtonBackground:UIColor{
        return accentColor
    }
    
    static var secondaryButtonBorder:UIColor{
        return mainButtonBackground
    }
    
    static var secondaryButtonDisabledBackground:UIColor{
        return .lightGray
    }
    
    static var secondaryButtonDissabledBorder:UIColor{
        return mainButtonDisabledBackground
    }
    
    //UITableView
    static var mainTableViewBackground:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    static var mainTableViewHeaderBackground:UIColor{
        return mainTableViewBackground
    }
    
    static var mainTableViewCellBackground:UIColor{
        return mainTableViewBackground
    }
    
    
    //Popup
    static var mainViewPopupBackground:UIColor{
        return UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.2)
    }
    
    static var viewPopupHeader:UIColor{
        return secondaryViewBackground//UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1)
    }
    
    static var viewInnerPopupBackground:UIColor{
        return mainViewBackground
    }
    
    static var mainPopupButtonBackground:UIColor{
        return accentColor
    }
    
    static var mainPopupButtonBorder:UIColor{
        return mainButtonBackground
    }
    
    static var mainPopupButtonDisabledBackground:UIColor{
        return UIColor(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1)
    }
    
    static var mainPopupButtonDissabledBorder:UIColor{
        return mainButtonDisabledBackground
    }
    
    static var secondaryPopupButtonBackground:UIColor{
        return secondaryViewBackground
    }
    
    static var secondaryPopupButtonBorder:UIColor{
        return secondaryPopupButtonBackground
    }
    
    static var secondaryPopupButtonDisabledBackground:UIColor{
        return secondaryPopupButtonBackground
    }
    
    static var secondaryPopupButtonDissabledBorder:UIColor{
        return mainButtonDisabledBackground
    }
    
    
    //MARK: Text color
    static var mainTextColor:UIColor{
        return UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1)
    }
    
    static var mainHighlightedTextColor:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    static var secondaryTextColor:UIColor{
        //Not applicable yet
        return UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1)
    }
    
    static var textFieldPlaceholder:UIColor {
        return UIColor(red: 120.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1)
    }
    
    static var popupTitleTextColor:UIColor{
        return mainTextColor
    }
    
    static var mainButtonTextColor:UIColor{
        return mainHighlightedTextColor
    }
    
    static var mainButtonDisabledTextColor:UIColor{
        return mainTextColor
    }
    
    static var secondaryButtonTextColor:UIColor{
        return mainTextColor
    }
    
    static var secondaryButtonDisabledTextColor:UIColor{
        return mainTextColor
    }
    
    static var mainPopupButtonTextColor:UIColor{
        return mainHighlightedTextColor
    }
    
    static var mainPopupButtonDisabledTextColor:UIColor{
        return mainTextColor
    }
    
    static var secondaryPopupButtonTextColor:UIColor{
        return mainTextColor
    }
    
    static var secondaryPopupButtonDisabledTextColor:UIColor{
        return mainTextColor
    }
    
    static var textFieldPopupPlaceholder:UIColor {
        return UIColor(red: 120.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1)
    }
    
    
    //UITableView
    static var popupTableViewBackground:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    static var popupTableViewHeaderBackground:UIColor{
        return mainTableViewBackground
    }
    
    static var popupTableViewCellBackground:UIColor{
        return mainTableViewBackground
    }
}

struct Margins {
    
    static let xSmall:CGFloat = 3
    static let small:CGFloat = 4
    static let medium:CGFloat = 8
    static let large:CGFloat = 12
    static let xLarge:CGFloat = 18
    static let xxLarge:CGFloat = 24
    
}

struct TextSize {
    
    static let navigationTitle:CGFloat = 17
    
    static let title:CGFloat = DeviceTraitStatus.current == .wRhR ? 21:18
    
    static let normal:CGFloat = DeviceTraitStatus.current == .wRhR ? 18:15
    static let normalSmall:CGFloat = DeviceTraitStatus.current == .wRhR ? 16:13
    
    static let heaader:CGFloat = DeviceTraitStatus.current == .wRhR ? 42:32
    static let subHeader:CGFloat = DeviceTraitStatus.current == .wRhR ? 29:21
    
}

struct BorderRadius {
    
    static let xSmall:CGFloat = 3
    static let small:CGFloat = 5
    static let medium:CGFloat = 10
    static let large:CGFloat = 15
    static let xlarge:CGFloat = 20
    static let xxlarge:CGFloat = 25
    
}

struct Separators {
    
    static let xSmall:CGFloat = 1
    static let small:CGFloat = 3
    static let medium:CGFloat = 6
    static let large:CGFloat = 8
    static let xlarge:CGFloat = 12
    static let xxlarge:CGFloat = 15
    
}

