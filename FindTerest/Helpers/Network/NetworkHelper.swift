//
//  NetworkHelper.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

//https://medium.com/swiftcraft/swift-solutions-singleton-27e932879dfb
class NetworkHelper: NSObject {
    
    //MARK:- Properties
    //MARK: Constants
    enum NetworkHelperError:Error{
        case invalidFourSquareConfig
        case invalidURL
        case noResponse
    }
    
    static let shared = NetworkHelper()
    let network:Network
    let foursquareConfig:FoursquareConfig?
    
    
    
    //MARK:- Constructor
    required init(network:Network = AppNetwork(), foursquareConfig:FoursquareConfig? = FoursquareConfig(fileName: fourSquareConfigPath)) {

        self.network = network
        self.foursquareConfig = foursquareConfig
        
        super.init()
        
    }
    
}


//MARK:- Public methods
extension NetworkHelper{
    
    func getVenueRecomendations(inLocation:String, offset:Int, limit:Int, radius:Int = 1000, completion:@escaping (Result<VenuesResponse, Error>)->Void){
        
        guard let foursquareConfig = foursquareConfig else{
            completion(.failure(NetworkHelper.NetworkHelperError.invalidFourSquareConfig))
            return
        }
        
        var urlComponents = URLComponents(string: network.venueRcomendations)
        
        urlComponents?.queryItems = foursquareConfig.queryItems
        urlComponents?.queryItems?.append(URLQueryItem(name: "near", value: inLocation))
        urlComponents?.queryItems?.append(URLQueryItem(name: "radius", value: String(radius)))
        urlComponents?.queryItems?.append(URLQueryItem(name: "offset", value: String(offset)))
        urlComponents?.queryItems?.append(URLQueryItem(name: "limit", value: String(limit)))
        urlComponents?.queryItems?.append(URLQueryItem(name: "sortByDistance", value: String(1)))
        
        guard let venuesURL = urlComponents?.url else{
            completion(.failure(NetworkHelper.NetworkHelperError.invalidURL))
            return
        }
                
        let venuesURLRequest = URLRequest(url: venuesURL, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 5)
        network.fetchCodable(urlRequest: venuesURLRequest) { [weak self] (_ venuesResult: Result<RecomendedVenuesSearchResult, Error>) in
            
            guard let strongSelf = self else{ return }
            switch venuesResult{
            case .success(let venuesRecResult):
                
                print(strongSelf.logClassName, "Venues URL Request success")
                if let response = venuesRecResult.response{
                    completion(.success(response))
                }
                else{
                    completion(.failure(NetworkHelper.NetworkHelperError.noResponse))
                }
                
                
            case .failure(let venueSearchError):
                completion(.failure(venueSearchError))
            }
            
        }
        
    }
    
}


//TODO

