//
//  AppHelper.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 04/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

//Colection of helpers

///Sets gloabally the navigation bar style
func setupNavigationBarStyle(){
    
    UINavigationBar.appearance().barTintColor = UIColor.navigationBarBackground
    UINavigationBar.appearance().tintColor = UIColor.navigationBarTint
    
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.navigationBarText]
    
    UINavigationBar.appearance().isTranslucent = false
    
    UITabBar.appearance().barTintColor = UIColor.navigationBarBackground
    UITabBar.appearance().tintColor = UIColor.navigationBarTint
    
}
