//
//  SearchViewViewModel.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import RxCocoa


class SearchViewViewModel: NSObject {

    //MARK:- Properties
    //MARK: Constants
    let disposeBag = DisposeBag()
    let appLocationManager:AppLocationManager
    
    
    //MARK: Vars
    var currentLocation:BehaviorRelay<CLLocation?>{
        return appLocationManager.currentLocation
    }
    var searchPlaceString = BehaviorRelay(value: "")
    var nearByCity:BehaviorRelay<String?> = BehaviorRelay(value: nil)
    var isNearBy = BehaviorRelay(value: true)
    var searchString:BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    
    //MARK:- Constructor
    init(appLocationManager:AppLocationManager = AppLocationManager.shared) {
    
        self.appLocationManager = appLocationManager
        super.init()
        
        setupBinders()
        
    }
    
}


//MARK:- Private methods
private extension SearchViewViewModel{
    
    func setupBinders(){
        
        nearByCity
            .asObservable()
            .distinctUntilChanged()
            .map({$0 ?? ""})
            .map({$0 != ""})
            .bind(to: isNearBy)
            .disposed(by: disposeBag)
        
        currentLocation
            .asObservable()
            .distinctUntilChanged()
            .filter({$0 != nil})
            .map({$0!})
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { (location) in
                
                AppLocationManager.getCity(from: location, completion: { [weak self] (result) in
                    
                    guard let strongSelf = self else{ return }
                    
                    switch result{
                    case .success(let city):
                        strongSelf.nearByCity.accept(city)
                    case .failure(let error):
                        print(strongSelf.logClassName, "Error getting location: ", error)
                    }
                    
                    
                })
                
            })
            .disposed(by: disposeBag)
        
        
    }
    
}
