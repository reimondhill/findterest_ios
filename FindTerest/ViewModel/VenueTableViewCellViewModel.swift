//
//  VenueTableViewCellViewModel.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 03/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit

//ViewModel Without RxSwift. A different approach.
class VenueTableViewCellViewModel: NSObject {
    
    let item:Item
    
    var venueIconURL:URL?{
        return item.venue?.categories?.first?.mediumIconURL
    }
    var distanceKMString:String{
        guard let distance = item.venue?.location?.distance else{ return "N/A" }
        
        let distKm = Double(distance) / 1000
        return "\(String(format: "%.1f", distKm)) km"
    }
    var nameVenue:String{
        return item.venue?.name ?? unknown
    }
    var reasonString:String{
        
        guard let summary = item.reason?.reassons.first?.summary,
            var reason = item.reason?.reassons.first?.reasonName else{ return ""}
        
        reason = reason.camelCaseToWords()
        reason = reason.replacingOccurrences(of: "Reason", with: "")
        reason = reason.lowercased()
        
        return "\(summary) for \(reason)"
    }
    
    var addressVenue:String{
        
        if let formattedAddress = item.venue?.location?.formattedAddress{
            return formattedAddress.joined(separator: ",")
        }
        else{
            return unknown
        }
       
    }
    
    init(item:Item) {
        
        self.item = item
        super.init()
        
    }
    
    
}
