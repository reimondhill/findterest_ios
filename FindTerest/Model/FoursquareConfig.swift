//
//  FoursquareConfig.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import Foundation

//Ideally data for configuration should be fetched from a backend. It is not secure to store sensible data on the phone
class FoursquareConfig: NSObject, Decodable{
   
    //MARK:- Properties
    //MARk: Constants
    enum CodingKeys: String, CodingKey {
        
        case clientID = "client_id"
        case clientSecret = "client_secret"
        case version = "v"
        
    }
    
    let clientID:String
    let clientSecret:String
    let version:String
    
    
    //MARK: Vars
    var getFormattedParams:String{
        return  "client_id=\(clientID)&client_secret=\(clientSecret)&v=\(version)"
    }
    var queryItems:[URLQueryItem]{
        var rtQueryItems = [URLQueryItem]()
        
        rtQueryItems.append(URLQueryItem(name: "client_id", value: clientID))
        rtQueryItems.append(URLQueryItem(name: "client_secret", value: clientSecret))
        rtQueryItems.append(URLQueryItem(name: "v", value: version))
        
        return rtQueryItems
    }
    
    override var description: String{
        return "clientID=\(clientID), clientSecret=\(clientSecret), version=\(version)"
    }
    
    
    //MARK:- Contructor extension
    init?(fileName:String) {
        
        if let fileURL = Bundle.main.url(forResource: fileName, withExtension: "json"),
            
            let fileData = try? Data(contentsOf: fileURL),
            let configuration = try? JSONDecoder().decode(FoursquareConfig.self, from: fileData){
        
            clientID = configuration.clientID
            clientSecret = configuration.clientSecret
            version = configuration.version
            
        }
        else{
            print("Error reading/parsing Foursquare dev configuration.")
            return nil
        }
        
        super.init()
        
    }
    
}
