//
//  AppLocationManager.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import RxCocoa


class AppLocationManager: NSObject {
    
    //MARK:- Properties
    //MARK: Constants
    enum LocationError:Error{
        case reverseGeocodeLocation
        case unknowCity
    }
    
    let disposeBag = DisposeBag()
    static let shared = AppLocationManager()
    
    
    //MARK: Vars
    var locationManager = CLLocationManager()
    var isSearching = BehaviorRelay(value: false)
    var currentLocation:BehaviorRelay<CLLocation?> = BehaviorRelay(value: nil)
    
}


//MARK:- Public methods
extension AppLocationManager{
    
    func startLocationUpdate(){
        
        if !isSearching.value{
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            
            isSearching.accept(true)
            locationManager.startUpdatingLocation()
            
        }
        
    }
    
    func stopLocationUpdate(){
        
        if isSearching.value{
        
            isSearching.accept(false)
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            
        }
        
    }
    
    static func getCity(from currentLocation:CLLocation, completion:@escaping (Result<String,Error>)->Void){
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
            
            if let error = error {
                print(logClassName, "Unable to Reverse Geocode Location -> ", error)
                completion(.failure(LocationError.reverseGeocodeLocation))
            }
            else{
                
                if let placemark = placemarks?.first,
                    let city = placemark.locality{
                    //print(logClassName, "Found city: ", city)
                    completion(.success(city))
                }
                else{
                    completion(.failure(LocationError.unknowCity))
                }
                
            }
            
        }
        
    }
    
}


//MARK:- CLLocationManagerDelegate implementation
extension AppLocationManager:CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard locations.count > 0 else{ return }
        currentLocation.accept(locations[0])
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print(logClassName, "Location manager did fail: ", error)
        isSearching.accept(false)
        
    }
    
}
