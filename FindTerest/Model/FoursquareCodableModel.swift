//
//  FoursquareCodableModel.swift
//  FindTerest
//
//  Created by Ramon Haro Marques on 04/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import Foundation


struct RecomendedVenuesSearchResult:Codable{
    
    let meta:Meta
    let response:VenuesResponse?
}

struct Meta:Codable {
    
    let code:Int
    let requestID:String

    enum CodingKeys: String, CodingKey {
        case code
        case requestID = "requestId"
    }
    
}

struct VenuesResponse:Codable {
    
    let warning:Warning?
    
    let suggestedRadius:Int?
    let headerLocation:String?
    let headerLocationGranularity:String?
    
    let suggestedBounds:SuggestedBounds?
    
    let totalResults:Int
    let groups:[Group]
    
}

struct Warning:Codable {
    let text:String?
}

struct SuggestedBounds:Codable{
    
    let ne:NorthEast?
    let sw:SouthWest?
    
}

struct NorthEast:Codable {
    let latitud:Double?
    let longitud:Double?
    
    enum CodingKeys: String, CodingKey {
        case latitud = "lat"
        case longitud = "lng"
    }
    
}

struct SouthWest:Codable {
    let latitud:Double?
    let longitud:Double?
    
    enum CodingKeys: String, CodingKey {
        case latitud = "lat"
        case longitud = "lng"
    }
    
}

struct Group: Codable{
    let type:String?
    let name:String?
    
    let items:[Item]?
    
    
}

struct Item:Codable{
    
    let reason:Reasson?
    let venue:Venue?
    
    enum CodingKeys: String, CodingKey {
        case reason = "reasons"
        case venue
    }
    
}

struct Reasson:Codable{
    let count:Int?
    let reassons:[ReasonItem]
    
    struct ReasonItem:Codable{
        let summary:String?
        let type:String
        let reasonName:String
    }
    
    enum CodingKeys: String, CodingKey {
        case count
        case reassons = "items"
    }
    
}

struct Venue:Codable {
    
    let id:String?
    let name:String?
    let location:Location?
    let categories:[Category]?
    
}

struct Location:Codable {
    
    let address:String?
    let crossStreet:String?
    
    let latitud:Double?
    let longitud:Double?
    let distance:Int?

    let countryCode:String?
    let country:String?
    let state:String?
    let city:String?
    let postalCode:String?
    let formattedAddress:[String]?
    
    enum CodingKeys: String, CodingKey {
        case address
        case crossStreet
        case latitud = "lat"
        case longitud = "lng"
        case distance
        case countryCode = "cc"
        case country
        case state
        case city
        case postalCode
        case formattedAddress
    }

}

struct Category:Codable{
    
    let id:String?
    let name:String?
    let pluralName:String?
    let shortName:String?
    let primary:Bool?
    
    let icon:Icon?
    var smallIconURL:URL?{
        guard let icon = icon,
            let prefix = icon.prefix,
            let suffix = icon.suffix,
            let iconURL = URL(string: "\(prefix)\(Icon.IconSize.small.rawValue)\(suffix)") else { return nil }
        
        return iconURL
    }
    var mediumIconURL:URL?{
        guard let icon = icon,
            let prefix = icon.prefix,
            let suffix = icon.suffix,
            let iconURL = URL(string: "\(prefix)\(Icon.IconSize.medium.rawValue)\(suffix)") else { return nil }
        
        return iconURL
    }
    var largeIconURL:URL?{
        guard let icon = icon,
            let prefix = icon.prefix,
            let suffix = icon.suffix,
            let iconURL = URL(string: "\(prefix)\(Icon.IconSize.large.rawValue)\(suffix)") else { return nil }
        
        return iconURL
    }
    var xLargeIconURL:URL?{
        guard let icon = icon,
            let prefix = icon.prefix,
            let suffix = icon.suffix,
            let iconURL = URL(string: "\(prefix)\(Icon.IconSize.xLarge.rawValue)\(suffix)") else { return nil }
        
        return iconURL
    }
    
    let venuPage:VenuPage?

    
}

struct Icon:Codable {
    
    enum IconSize:Int {
        case small = 32
        case medium = 44
        case large = 64
        case xLarge = 88
    }
    
    let prefix:String?
    let suffix:String?
    
}

struct VenuPage:Codable {
    let id:String?
}


