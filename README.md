# FindTerest - Start discovering the best places 

FindTerest is a mobile apllication for iOS inspired by *Foursquare city guide*. The app works on iOS11+ devices and it supports both iPhone and iPad portrait design.

FindTerest allows the user to explore venues in places of their choice or nearby automated current location.

The app is coded using Swift 5 and with some of the latest version techniques such as *Result Types*, amongst others. In addition, the code is structured in a way that offers the possibilty to scale easily and to be tested easily using *MVVM*, *Dependency Injection* and *Protocol Oriented Programing*. 

The app also offers a starting set of both Unit and UI tests grouped within differnet Swift files with the possibilty to add more in the future for great *Continuos Integration*.

The API's used for this starting project are *Foursquare* and *CoreLocation*. Deficiencies of the API include the foursquare account being a free account, not providing full results and it being extremely inneficient to fetch venue images. For that reason, this starting project displays the necessary inportant information with potential for further development. 

For this project *Dependency Manager* is used using *Carthage* with great and useful third party libraries including RxSwift, RxCocoa and SDWebImage.


## App flow

### SplashScreen
![Alt text](UIResources/Screenshots/sc1.PNG?raw=true)


### MainSearchViewController
![Alt text](UIResources/Screenshots/sc2.PNG?raw=true)


![Alt text](UIResources/Screenshots/sc3.PNG?raw=true)


### SearchResultViewController
![Alt text](UIResources/Screenshots/sc4.PNG?raw=true)


![Alt text](UIResources/Screenshots/sc5.PNG?raw=true)


![Alt text](UIResources/Screenshots/sc6.PNG?raw=true)


## Future development

* Improvement of the UI
* Ability to look for venues by categories on the MainSearchViewController
* UITabView with more options
* Venue details view controller
* Navigation to places
* Register class for AnnotationView in VenuesMapView
* Further tests
* Fix possible bugs


## Recent projects
[Ramon Haro Marques Convert technologies projects](UIResources/RamonHaroMarques_ConvertTechnologiesProjects.pdf)



## Licence

MIT License

Copyright (c) 2019 Ramon Haro Marqu�s.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
