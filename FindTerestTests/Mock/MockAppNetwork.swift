//
//  MockAppNetwork.swift
//  FindTerestTests
//
//  Created by Ramon Haro Marques on 10/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import Foundation
@testable import FindTerest

//MARK:- Network implementation
class MockAppNetwork:Network{
    
    var venueRcomendations: String {
        guard let url = Bundle.main.url(forResource: "RecomendedVenuesResponse", withExtension: "json") else{ return ""}
        return url.absoluteString
    }
    var venueSearch:String{
        return "TODO"
    }
    var venueDetails:String{
        return "TODO"
    }
    
    
    func fetchCodable<T>(urlRequest: URLRequest, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable, T : Encodable {
        
        do{
            let response = try JSONDecoder().decode(T.self, from: try Data(contentsOf: urlRequest.url!))
            completion(.success(response))
        }
        catch{
            print("Error fetching data", error.localizedDescription)
            completion(.failure(error))
        }
        
    }
    
    func fetchData(urlRequest: URLRequest, completion: @escaping (Result<Data, Error>) -> Void) {
        
        //TODO
        
    }
    
    func send(data: Data, urlRequest: URLRequest, completion: @escaping (Error?) -> Void) {
        
        //TODO
        
    }
    
}
