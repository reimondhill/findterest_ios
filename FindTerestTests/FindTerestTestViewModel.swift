//
//  FindTerestTestViewModel.swift
//  FindTerestTests
//
//  Created by Ramon Haro Marques on 11/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import XCTest
@testable import FindTerest

import Foundation
import RxSwift
import RxCocoa
import RxTest


//Testing RxSwift
class FindTerestTestViewModel: XCTestCase {
    
    let disposeBag = DisposeBag()
    //Testing SearchViewViewModel IsNearBy
    func testSearchViewViewModelIsNearBy(){
        
        let scheduler = TestScheduler(initialClock: 0)
        let source = scheduler.createColdObservable([.next(5, "London"), .next(10, ""), .completed(10)])
        let sink = scheduler.createObserver(Bool.self)
        let disposeBag = DisposeBag()
        
        let viewModel = SearchViewViewModel()
        source.bind(to: viewModel.nearByCity).disposed(by: disposeBag)
        viewModel.isNearBy.bind(to: sink).disposed(by: disposeBag)
        
        scheduler.start()
        
        XCTAssertEqual(sink.events, [.next(0, false), .next(5, true), .next(10, false)])
        
    }
    
}

extension TestScheduler {
    /// Creates a `TestableObserver` instance which immediately subscribes
    /// to the `source` and disposes the subscription at virtual time 100000.
    func record<O: ObservableConvertibleType>(_ source: O) -> TestableObserver<O.Element> {
        let observer = self.createObserver(O.Element.self)
        let disposable = source.asObservable().bind(to: observer)
        self.scheduleAt(100000) {
            disposable.dispose()
        }
        return observer
    }
}
