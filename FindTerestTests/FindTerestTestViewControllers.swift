//
//  FindTerestTestViewControllers.swift
//  FindTerestTests
//
//  Created by Ramon Haro Marques on 05/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import XCTest
@testable import FindTerest


class FindTerestTestViewControllers: XCTestCase {
    
    func testSearchViewController(){
        
        let viewController = SearchResultViewController(cityString: "London")
        
        XCTAssertEqual(viewController.preferredStatusBarStyle, .lightContent)
        XCTAssertEqual(viewController.indicator.superview,viewController.view)
        
    }
        
}
