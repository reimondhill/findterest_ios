//
//  FindTerestTestModelAndNework.swift
//  FindTerestTests
//
//  Created by Ramon Haro Marques on 05/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//


import XCTest
import CoreLocation
@testable import FindTerest

class FindTerestTestModelAndNework: XCTestCase {
    
    //Foursquare configuration
    func testCheckForsquareInitialisationAndParam(){
        
        guard let foursquareConfig = FoursquareConfig(fileName: "FourSquareDevData") else{
            XCTFail()
            return
        }
        
        XCTAssert(foursquareConfig.getFormattedParams == "client_id=\(foursquareConfig.clientID)&client_secret=\(foursquareConfig.clientSecret)&v=\(foursquareConfig.version)")
        
        XCTAssert(foursquareConfig.description == "clientID=\(foursquareConfig.clientID), clientSecret=\(foursquareConfig.clientSecret), version=\(foursquareConfig.version)")
        
        let queryItem = foursquareConfig.queryItems
        if let cliendIDIndex = queryItem.firstIndex(where: {$0.name == "client_id"}),
            let clientSecret = queryItem.firstIndex(where: {$0.name == "client_secret"}),
            let version = queryItem.firstIndex(where: {$0.name == "v"}){
            
            XCTAssertEqual(queryItem[cliendIDIndex].value, foursquareConfig.clientID)
            XCTAssertEqual(queryItem[clientSecret].value, foursquareConfig.clientSecret)
            XCTAssertEqual(queryItem[version].value, foursquareConfig.version)
            
        }
        else{
            
            XCTFail()
            
        }
        
        
    }
    
    //Check MockResponse Exists
    func getMockRecomendedVeuesJSON()->Data?{
        
        guard let fileURL = Bundle.main.url(forResource: "RecomendedVenuesResponse", withExtension: "json"),
            let fileData = try? Data(contentsOf: fileURL) else{
                return nil
        }
        
        return fileData
        
    }
    
    //Codable implementation
    func testRecomendedVeuesCodable(){
        
        if let fileData = getMockRecomendedVeuesJSON(){
            
            do{
                let response = try JSONDecoder().decode(RecomendedVenuesSearchResult.self, from: fileData)
                
                print(response.response?.warning?.text ?? "NONE")
                
                if let smallIconURL = response.response?.groups.first?.items?.first?.venue?.categories?.first?.smallIconURL{
                    XCTAssertEqual(smallIconURL.absoluteString, "https://ss3.4sqi.net/img/categories_v2/food/steakhouse_32.png")
                }
                if let mediumIconURL = response.response?.groups.first?.items?.first?.venue?.categories?.first?.mediumIconURL{
                    XCTAssert(mediumIconURL.absoluteString == "https://ss3.4sqi.net/img/categories_v2/food/steakhouse_44.png")
                }
                if let largeIconURL = response.response?.groups.first?.items?.first?.venue?.categories?.first?.largeIconURL{
                    XCTAssert(largeIconURL.absoluteString == "https://ss3.4sqi.net/img/categories_v2/food/steakhouse_64.png")
                }
                if let xLargeIconURL = response.response?.groups.first?.items?.first?.venue?.categories?.first?.xLargeIconURL{
                    XCTAssert(xLargeIconURL.absoluteString == "https://ss3.4sqi.net/img/categories_v2/food/steakhouse_88.png")
                }
                
            }
            catch{
                print("RecomendedVenuesResponseDecoded FAILED ", error)
                XCTFail()
            }
            
        }
        else{
            print("Unable to find testing files")
            XCTFail()
        }
        
    }
    
    //Check Network implementation
    func testCheckNetworkHelper(){
        
        let networkHelper = NetworkHelper(network:MockAppNetwork())
        networkHelper.getVenueRecomendations(inLocation: "London", offset: 0, limit: 1) { (result) in
            
            switch result{
            case .success(_):
                break
            case .failure(_):
                XCTFail()
            }
            
            
        }
        
    }
    
    //AppLocationManager get cityFromCordinates
    func testAppLocationManagerFromLocation(){
        
        let londonCoordinates = CLLocation(latitude: CLLocationDegrees(51.5074), longitude: CLLocationDegrees(0.1278))
        
        AppLocationManager.getCity(from: londonCoordinates) { (result) in
            
            switch result{
            case .success(let cityString):
                XCTAssertEqual(cityString, "London")
            case .failure(_):
                XCTFail()
            }
            
        }
        
    }
    
}
