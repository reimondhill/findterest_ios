//
//  FindTerestTestViews.swift
//  FindTerestTests
//
//  Created by Ramon Haro Marques on 05/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import XCTest
@testable import FindTerest

class FindTerestTestViews: XCTestCase {
    
    //MARK:- BaseViews
    func testBaseLabelColorFont(){
        
        BaseLabel.LabelConfiguration.allCases.forEach { (value) in
            
            let baseLabel = BaseLabel(withConfiguration: value)
            switch value{
            case .normal:
                XCTAssert(baseLabel.textColor == UIColor.mainTextColor)
                XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.normal, weight: .regular))
                XCTAssert(baseLabel.labelConfigurationOption == 0)
                XCTAssert(baseLabel.labelConfiguration == BaseLabel.LabelConfiguration.normal)
            case .normalSmall:
                XCTAssert(baseLabel.textColor == UIColor.mainTextColor)
                XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.normalSmall, weight: .light))
                XCTAssert(baseLabel.labelConfigurationOption == 1)
                XCTAssert(baseLabel.labelConfiguration == BaseLabel.LabelConfiguration.normalSmall)
            case .normalHigligthed:
                XCTAssert(baseLabel.textColor == UIColor.mainHighlightedTextColor)
                XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.normal, weight: .regular))
                XCTAssert(baseLabel.labelConfigurationOption == 2)
                XCTAssert(baseLabel.labelConfiguration == BaseLabel.LabelConfiguration.normalHigligthed)
            case .normalSmallHigligthed:
                XCTAssert(baseLabel.textColor == UIColor.mainHighlightedTextColor)
                XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.normalSmall, weight: .light))
                XCTAssert(baseLabel.labelConfigurationOption == 3)
                XCTAssert(baseLabel.labelConfiguration == BaseLabel.LabelConfiguration.normalSmallHigligthed)
            case .navBar:
                XCTAssert(baseLabel.textColor == UIColor.navigationBarText)
                XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.navigationTitle, weight: .semibold))
                XCTAssert(baseLabel.labelConfigurationOption == 4)
                XCTAssert(baseLabel.labelConfiguration == BaseLabel.LabelConfiguration.navBar)
            case .header:
                XCTAssert(baseLabel.textColor == UIColor.navigationBarText)
                XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.heaader, weight: .heavy))
                XCTAssert(baseLabel.labelConfigurationOption == 5)
                XCTAssert(baseLabel.labelConfiguration == BaseLabel.LabelConfiguration.header)
            case .subHeader:
                XCTAssert(baseLabel.textColor == UIColor.mainHighlightedTextColor)
                XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.subHeader, weight: .semibold))
                XCTAssert(baseLabel.labelConfigurationOption == 6)
                XCTAssert(baseLabel.labelConfiguration == BaseLabel.LabelConfiguration.subHeader)
            case .title:
                XCTAssert(baseLabel.textColor == UIColor.mainTextColor)
                XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.title, weight: .bold))
                XCTAssert(baseLabel.labelConfigurationOption == 7)
                XCTAssert(baseLabel.labelConfiguration == BaseLabel.LabelConfiguration.title)
            }
            
        }
        
        
    }
    
    func testBaseLabelSetters(){
        
        let baseLabel = BaseLabel(withConfiguration: .normal)
        baseLabel.labelConfigurationOption = 1
        XCTAssert(baseLabel.labelConfiguration == .normalSmall)
        
        XCTAssert(baseLabel.textColor == UIColor.mainTextColor)
        XCTAssert(baseLabel.font == UIFont.systemFont(ofSize: TextSize.normalSmall, weight: .light))
        XCTAssert(baseLabel.labelConfigurationOption == 1)
        
    }
    
    func testBaseTableView(){
        
        let baseTableView = BaseTableView(frame: .zero)
        
        XCTAssert(baseTableView.bounces == false)
        XCTAssert(baseTableView.backgroundColor == UIColor.mainTableViewBackground)
        
    }
    
}
