//
//  FindTerestTestExtensions.swift
//  FindTerestTests
//
//  Created by Ramon Haro Marques on 03/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import XCTest
@testable import FindTerest


//Extensions will be tested in this file

class MockClass:NSObject{
}

class FindTerestTestExtensions: XCTestCase {
    
    //MARK:- NSObjectExtension
    func testNSObjectLogClassName(){
        
        let mockClass = MockClass()
        XCTAssertEqual(mockClass.logClassName,"MockClass:")
        XCTAssertEqual(MockClass.logClassName,"MockClass:")
        
    }
    
    func testNSObjectIdentifier(){
        
        let mockClass = MockClass()
        XCTAssertEqual(mockClass.identifier, "MockClass")
        XCTAssertEqual(MockClass.identifier, "MockClass")
        
    }
    
    
    //MARK:- StringExtension
    func testStringCapitaliseFirstLetter(){
        
        let testString = "test".capitalizingFirstLetter()
        XCTAssert(testString == "Test")
        
    }
    
    func testStringCamelCaseToWords(){
        
        let camelCase = "camelCase".camelCaseToWords()
        XCTAssertEqual(camelCase,"camel Case")
        
    }
    
    
    //MARK:- UIImageExtension
    func testUIImageAspectRatio(){
        
        var image = UIImage(named: "main_search_header_image")!
        XCTAssert(image.aspectRatioValue == image.size.width/image.size.height)
        
        image = UIImage()
        XCTAssert(image.aspectRatioValue == 0)
        
    }

    
    //MARK:- UIImageExtension
    func testUIViewControllerCurrentTraitStatus(){
        
        //Testing only Portait
        let viewController = UIViewController()
        switch (UIScreen.main.traitCollection.horizontalSizeClass, UIScreen.main.traitCollection.verticalSizeClass){
            
        case (UIUserInterfaceSizeClass.regular, UIUserInterfaceSizeClass.regular):
            return XCTAssertEqual(viewController.currentTraitStatus, .wRhR)
        case (UIUserInterfaceSizeClass.compact, UIUserInterfaceSizeClass.regular):
            return XCTAssertEqual(viewController.currentTraitStatus, .wChR)
            
        default:
            break
        }
        
    }
    
    
    //MARK:- UIViewExtension
    func testUIViewConstraintToSuperViewEdges(){
        
        let containerView = UIView(frame: CGRect(origin: .zero, size: .init(width: 50, height: 50)))
        
        let innerView = UIView()
        containerView.addSubview(innerView)
        let anchoredConstraints = innerView.constraintToSuperViewEdges()
        
        XCTAssert(innerView.translatesAutoresizingMaskIntoConstraints == false)
        XCTAssert(anchoredConstraints != nil)
        XCTAssert(anchoredConstraints?.top != nil && anchoredConstraints?.trailing != nil && anchoredConstraints?.bottom != nil && anchoredConstraints?.leading != nil)
        
    }
    
    func testUIViewAnchor(){
        
        let containerView = UIView(frame: CGRect(origin: .zero, size: .init(width: 50, height: 50)))
        
        let innerView = UIView()
        containerView.addSubview(innerView)
        let anchoredConstraints = innerView.anchor(top: containerView.topAnchor,
                                                   leading: containerView.leadingAnchor,
                                                   bottom: containerView.bottomAnchor,
                                                   trailing: containerView.trailingAnchor,
                                                   size: .init(width: 0, height: 0))
        
        XCTAssert(innerView.translatesAutoresizingMaskIntoConstraints == false)
        XCTAssert(anchoredConstraints.top != nil && anchoredConstraints.trailing != nil && anchoredConstraints.bottom != nil && anchoredConstraints.leading != nil)
        XCTAssert(anchoredConstraints.width == nil && anchoredConstraints.height == nil)
        
        
    }
    
    func testCenterInSuperview(){
        
        let containerView = UIView(frame: CGRect(origin: .zero, size: .init(width: 50, height: 50)))
        
        let innerView = UIView()
        containerView.addSubview(innerView)
        let anchoredConstraints = innerView.centerInSuperview(size: .init(width: 25, height: 25))
        
        XCTAssert(innerView.translatesAutoresizingMaskIntoConstraints == false)
        XCTAssert(anchoredConstraints.top == nil && anchoredConstraints.trailing == nil && anchoredConstraints.bottom == nil && anchoredConstraints.leading == nil)
        XCTAssert(anchoredConstraints.width != nil && anchoredConstraints.height != nil)
        XCTAssert(anchoredConstraints.centerX != nil && anchoredConstraints.centerY != nil)
        
        
    }
    
    func testUIViewBorders(){
        
        let directions:[Direction] = [.up, .left, .down, .right]
        let view = UIView(frame: CGRect(origin: .zero, size: .init(width: 50, height: 50)))
        let color = UIColor.red
        let size:CGFloat = 1
        
        for direction in directions{
            var borderView:UIView = UIView()
            
            switch direction{
            case .up:
                borderView = view.addTopSeparator(color: color, height: size, margins: 0)
            case .left:
                borderView = view.addLeftSeparator(color: color, width: size, margins: 0)
            case .down:
                borderView = view.addBottomSeparator(color: color, height: size, margins: 0)
            case .right:
                borderView = view.addRightSeparator(color: color, width: size, margins: 0)
            default:
                return
                
            }
            
            XCTAssertEqual(borderView.backgroundColor, color)
            XCTAssertEqual(borderView.superview, view)
            
        }
        
    }
    
    func testUIViewParentViewController(){
        
        let viewController = UIViewController()
        let aView = UIView()
        viewController.view.addSubview(aView)
        
        XCTAssert(aView.viewControler != nil)
        XCTAssertEqual(aView.viewControler, viewController)
        
        XCTAssert(aView.findViewController() != nil)
        XCTAssertEqual(aView.findViewController(), viewController)
        
    }
    
}
