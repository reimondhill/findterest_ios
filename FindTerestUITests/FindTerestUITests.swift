//
//  FindTerestUITests.swift
//  FindTerestUITests
//
//  Created by Ramon Haro Marques on 02/06/2019.
//  Copyright © 2019 Reimond Hill. All rights reserved.
//

import XCTest

class FindTerestUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    ///Simple UITest testing UI and SearchButton enabled
    
    func testSearchViewController() {
    
        let app = XCUIApplication()
       
        let headerLabel = app.staticTexts["Header Label"]
        XCTAssertTrue(headerLabel.exists)
        XCTAssertEqual(headerLabel.label, "Start discovering the best places")

        let headerImageView =  app.otherElements.containing(.navigationBar, identifier:"FindTerest.MainSearchView").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        XCTAssertTrue(headerImageView.exists)
        
        print("Waiting to get current location")
        sleep(10)
        print("Testing button enabled")
        
        let textField = app.textFields["Search Locations Input"]
        let searchButton = app.buttons["Search Locations Button"]

        if textField.placeholderValue == "Search near by "{
            XCTAssertTrue(!searchButton.isEnabled)
            textField.tap()
            textField.typeText("London")
            XCTAssertTrue(searchButton.isEnabled)
        }
        else{
            XCTAssertTrue(searchButton.isEnabled)
            textField.tap()
            textField.typeText("London")
            XCTAssertTrue(searchButton.isEnabled)
        }
        
    }

}
